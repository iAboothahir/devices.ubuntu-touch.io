---
maturity: .6
---

The Samsung Galaxy Note 4 in variants 910F, 910P, 910T; codename `trlte, trltespr, trltetmo`.

### Preparatory steps

You can install Ubuntu Touch on the Samsung Galaxy Note 4 910F.

1. Make sure your device is [Unlocked](https://forum.xda-developers.com/t/howto-bootloader-unlock-and-upgrade-to-marshmallow-n910vvru2cql1.3398144/) if it is a Verizon Variant.
2. Ensure you have [Installed TWRP](https://twrp.me/samsung/samsunggalaxynote4qualcomm.html).
3. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) in the Android settings.
4. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking.
