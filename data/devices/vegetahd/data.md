---
name: "Bq Aquaris E5 HD"
deviceType: "phone"
description: "A solid entry-level smartphone. The Bq E5 HD was one of the first commercially available Ubuntu Touch devices!"
subforum: "72/bq-e5"

deviceInfo:
  - id: "cpu"
    value: "Arm Cortex-A7 Quadcore @1300MHz"
  - id: "chipset"
    value: "MediaTek MT6582"
  - id: "gpu"
    value: "ARM Mali-400 MP2 Dual Core @500MHz"
  - id: "rom"
    value: "16 GB"
  - id: "ram"
    value: "1 GB"
  - id: "android"
    value: "Android 4.0, upgradable to Android 4.4.2, ubuntu edition Ubuntu 15.04"
  - id: "battery"
    value: "Li-Po 2500 mAh"
  - id: "display"
    value: "5 inches, 68.97 cm2 (~68,58% screen-to-body ratio), 750 x 1280 pixels, 16:9 ratio (~294 ppi density)"
  - id: "rearCamera"
    value: "13MP 4128x3096, 2MP 1920x1080@30fps"
  - id: "frontCamera"
    value: "5MP 2560x1920, 0.3MP 640x480@30fps"
  - id: "arch"
    value: "armV7"
  - id: "dimensions"
    value: "142 x 71 x 8,7 mm (5.6 x 2.8 x 0.34 in)"
  - id: "weight"
    value: "134 g (4.73 oz)"

contributors:
  - name: "BQ"
    role: "Phone maker"
    expired: true
---
