import { componentScriptLoadAll } from "@logic/client/clientNavigation.js";
import { SidebarElement, SidebarService } from "sidebarjs";

export default function registerSidebars() {
  componentScriptLoadAll("[sidebarjs]", (e) => {
    e.classList.remove("d-none");

    var sidebar = new SidebarElement({
      onOpen: function () {
        if (e.dataset.trackingname)
          _paq.push([
            "trackEvent",
            "Sidebar",
            e.dataset.trackingname,
            window.location.pathname
          ]);
      },
      component: e,
      position: "right",
      nativeSwipeOpen: false
    });
  });
}
