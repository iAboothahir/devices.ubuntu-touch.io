// Netlify redirects file
// Outputs: _redirects

import releasesData from "@data/releases.json";

import * as ghUtils from "@logic/build/githubHelpers.js";

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

// Convert redirects object to string
function generateRedirectsFile(redirects) {
  let fileContent = "";
  for (let el of redirects) {
    fileContent += el.from + "    " + el.to + "    " + el.status + "\n";
  }
  return fileContent;
}

// Get installer URLs for download from Github
async function getInstallerUrls(netlifyRedirects) {
  try {
    const response = await ghUtils.avoidRateLimiting(
      "https://api.github.com/repos/ubports/ubports-installer/releases/latest"
    );
    const githubLinks = await response.json();

    // Save links to NetlifyRedirects collection
    for (let el of ["exe", "deb", "dmg", "appimage"]) {
      let targetUrl = getInstallerUrlForPackage(el, githubLinks);
      netlifyRedirects.push({
        from: "/installer package=" + el,
        to: targetUrl,
        status: "302!"
      });
      netlifyRedirects.push({
        from: "/installer/" + el,
        to: targetUrl,
        status: "302"
      });
    }
  } catch (e) {
    // Display a warning if installer data wasn't downloaded
    if (process.argv.includes("offline")) {
      console.log(
        ansiCodes.yellowFG + "%s" + ansiCodes.reset,
        "Failed to get installer download link!\n",
        "- If you need to fetch installer download link connect to the internet."
      );
    } else {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Failed to get installer download link!\n",
        "- If you want to ignore this error add the 'offline' parameter."
      );
      process.exit(2);
    }
  }

  // The link for the snap package is not stored at Github ( direct link is provided )
  netlifyRedirects.push({
    from: "/installer package=snap",
    to: "https://snapcraft.io/ubports-installer",
    status: "302!"
  });
  netlifyRedirects.push({
    from: "/installer/snap",
    to: "https://snapcraft.io/ubports-installer",
    status: "302"
  });

  return netlifyRedirects;
}

// Get links from Github
function getInstallerUrlForPackage(packageType, githubLinks) {
  try {
    return githubLinks.assets.find((asset) =>
      asset.name.toLowerCase().endsWith(packageType.toLowerCase())
    ).browser_download_url;
  } catch (e) {
    console.log(
      ansiCodes.redFG + "%s" + ansiCodes.reset,
      "Failed to get installer download link!\n"
    );
    process.exit(2);
  }
}

// Redirect current release long urls to short urls
function redirectDefaultRelease(netlifyRedirects) {
  netlifyRedirects.push({
    from: "/device/:device/release/" + releasesData.default + "/*",
    to: "/device/:device/:splat",
    status: "302"
  });

  return netlifyRedirects;
}

export async function get() {
  let redirects = [
    {
      from: "/about/categories",
      to: "https://docs.ubports.com/en/latest/porting/introduction/Intro.html",
      status: "301"
    }
  ];
  redirects = await getInstallerUrls(redirects);
  redirects = redirectDefaultRelease(redirects);

  return {
    body: generateRedirectsFile(redirects)
  };
}
